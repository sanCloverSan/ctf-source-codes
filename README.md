# ctf-source-codes

Source codes from CTFs

Some of it require deployment using Docker.

Some of it just a script that you can run.

Some of these challenges have already been solved (you can find the solutions here: [sancloversan](https://sancloversan.xyz/)).

PS: some of it may be considered as malware. Before running the script you better understand the code first.
